package com.openclassrooms.watchlist.repository;

import com.openclassrooms.watchlist.domain.WatchlistItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;
import java.util.Map;


/**
** Moved list of watchlistitems as instance variable here because that is our "DataBase", added CRUD operations because this is the DAL, also moved the methods of Util here and made them non static, these methods will also be interacting with the "DB"
 */

/**
 ** This is a repository class, which makes up the DAL (Data Access Layer)
 *! We want to keep this layer as thin as possible
 ** Responsibilities:
 ** 1) Class is limited to CRUD operations on a data source, usually a DB (class interacts with the DB)
 ** 2) manipulating Model object, which is then passed to the view component of the presentation layer, remember the presentation layer UI is made up of Model View Controller (MVC)
 **
 ** 3) called by layer above, the service/business layer which is made up of Service classes
 ** 4) return Model objects to service layer
 */


//@Repository
public class WatchlistitemRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;



    public List<WatchlistItem> getList(){
                  return jdbcTemplate.query("select * from Movies", (rs, rowNum) -> {
                      return new WatchlistItem(rs.getInt("id"),
                              rs.getString("title"),
                              rs.getString("rating"),
                              rs.getString("priority"),
                              rs.getString("comment"));
                  }      ) ;
                }


     //TODO test
    public void addItem(WatchlistItem watchlistItem){
        //watchlistItem.setId(WatchlistItem.index++);

        //TODO somehow we need to pass user id of whichever user is inserting the watchlist item
        int hardCodedTempUserId =1;
        jdbcTemplate.update("insert into Movies (id, title, rating, priority, comment,user_id) values (?,?,?,?,?,?)", new Object[]{watchlistItem.getId(), watchlistItem.getTitle(), watchlistItem.getRating(), watchlistItem.getPriority(), watchlistItem.getComment(), hardCodedTempUserId});
    };

    //TODO TEST
    public void updateItem(WatchlistItem watchlistItem, WatchlistItem existingWatchlistItem){
        jdbcTemplate.update("update Movies set title = ?, comment =?, priority =?, rating =? where id=?", new Object[] {watchlistItem.getTitle(), watchlistItem.getComment(), watchlistItem.getPriority(), watchlistItem.getRating(), existingWatchlistItem.getId()});
    }



    //TODO test
    //TODO, we actually want to find the watchlistItem by id for a specific user, for the user that is logged in, to be implemented
    public  WatchlistItem findWatchlistItemById(Integer id){
        return jdbcTemplate.queryForObject("select * from Movies where id = ?", ((rs, i) -> {
            WatchlistItem watchlistItem = new WatchlistItem();
            watchlistItem.setRating(rs.getString("rating"));
            watchlistItem.setComment(rs.getString("comment"));
            watchlistItem.setPriority(rs.getString("priority"));
            watchlistItem.setTitle(rs.getString("title"));
            watchlistItem.setId(rs.getInt("id"));
            return watchlistItem;

        }), id);

    }


    public boolean findIfWatchlistExists(Integer id) {
        String sql = "SELECT count(*) FROM table WHERE id = ?";
        boolean result = false;
        int count = jdbcTemplate.queryForObject(sql, new Object[] { id }, Integer.class);
        if (count == 1) {
            result = true;
        }
        return result;
    }

    public boolean findIfWatchlistExists(String title) {
        String sql = "SELECT count(*) FROM Movies WHERE title = ?";
        boolean result = false;
        int count = jdbcTemplate.queryForObject(sql, new Object[] { title }, Integer.class);
        if (count == 1) {
            result = true;
        }
        return result;
    }



    public WatchlistItem findByTitle(String title){
         return jdbcTemplate.queryForObject("select * from Movies where title = ?", ((rs, i) -> {
            WatchlistItem watchlistItem = new WatchlistItem();
            watchlistItem.setRating(rs.getString("rating"));
            watchlistItem.setComment(rs.getString("comment"));
            watchlistItem.setPriority(rs.getString("priority"));
            watchlistItem.setTitle(rs.getString("title"));
            watchlistItem.setId(rs.getInt("id"));
            return watchlistItem;

        }),title );
    }



}
