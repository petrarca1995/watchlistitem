package com.openclassrooms.watchlist.actuator;
import com.openclassrooms.watchlist.service.MovieRatingService;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;


//@Component not needed since we define class as bean in AppConfig.java
public class MovieRatingServiceHealthChecker implements HealthIndicator {

    private MovieRatingService movieRatingServiceImpl;

//    @Autowired
    public MovieRatingServiceHealthChecker(MovieRatingService movieRatingService){
        this.movieRatingServiceImpl = movieRatingService;
    }

    @Override
    public Health health() {

        if(movieRatingServiceImpl.getMovieRating("Up")==null){
            return Health.down().withDetail("cause","OMDb API is down").build();
        }

        return Health.up().build();
    }
}
