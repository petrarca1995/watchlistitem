package com.openclassrooms.watchlist.service.impl;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.openclassrooms.watchlist.service.MovieRatingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

//* service acts as a client for the OMDB API

//@Service("MovieRatingServiceLiveImpl")
//@Service
//@Profile("prod")
@Slf4j
public class MovieRatingServiceLiveImpl implements MovieRatingService {

    private String apiUrl = "http://www.omdbapi.com/?apikey=ac58dccb&t=";

    @Override
    public String getMovieRating(String title) {

        try {
            RestTemplate template = new RestTemplate();

            log.debug("OMDb API hit at url: "+ apiUrl+ " with title "+title);
            //** getForEntity used to make a GET request
            ResponseEntity<ObjectNode> response =
                    template.getForEntity(apiUrl + title, ObjectNode.class);

            ObjectNode jsonObject = response.getBody();


            return jsonObject.path("imdbRating").asText();
        } catch(NullPointerException e){
            log.warn("No rating available for this movie");
            return null;
        }
        catch (Exception e) {
            log.error("Something went wrong while calling OMDb API " + e.getMessage());
            return null;
        }
    }

}
