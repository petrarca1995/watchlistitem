package com.openclassrooms.watchlist.service.impl;

import com.openclassrooms.watchlist.service.MovieRatingService;

//@Profile("dev")
//@Service
public class MovieRatingServiceDummyImpl implements MovieRatingService {

    @Override
    public String getMovieRating(String title) {
        return "9";
    }
}
