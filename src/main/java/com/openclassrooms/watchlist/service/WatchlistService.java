package com.openclassrooms.watchlist.service;

import com.openclassrooms.watchlist.domain.WatchlistItem;
import com.openclassrooms.watchlist.exception.DuplicateTitleException;
import com.openclassrooms.watchlist.repository.WatchlistitemRepository;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

//@Service
public class WatchlistService {

    /**
    *! having these lines of code makes the WatchlistService class tightly coupled with the Watchlist*
    *! having the new keyword, which instantiates an instance of this classes dependencies makes the classes tightly coupled to each other because we are instantiating a real instance of the dependencies.
     *! which makes this class difficult to unit test
     *! there is no way to replace the real instance with a mock at the time of unit testing because when we create an instance of this class, we are creating a real instance of the dependencies
     */
//    private WatchlistitemRepository watchlistitemRepository =  new WatchlistitemRepository();
//    private MovieRatingService movieRatingService = new MovieRatingService();


    private WatchlistitemRepository watchlistitemRepository;
    private MovieRatingService movieRatingServiceImpl;

//    @Autowired
    public WatchlistService(WatchlistitemRepository watchlistitemRepository, MovieRatingService movieRatingService){
        this.watchlistitemRepository = watchlistitemRepository;
        this.movieRatingServiceImpl = movieRatingService;
    }


    public List<WatchlistItem> getWatchlist (){
        return watchlistitemRepository.getList();
    }

    public int getWatchlistSize(){
        return watchlistitemRepository.getList().size();
    }

    public void addOrUpdateWatchlistItems(WatchlistItem watchlistItem) throws DuplicateTitleException{
        if(watchlistItem.getId()==null){
            if (watchlistitemRepository.findIfWatchlistExists(watchlistItem.getTitle())){
                throw new DuplicateTitleException();
            }

            String rating = movieRatingServiceImpl.getMovieRating(watchlistItem.getTitle());

            //TODO, API only called when id =null so when we add new items initially, if we update an item, api not called
            //not sure why rating is empty when API returns movie not found message
            if (rating!= null &&  !rating.equals("")){
                watchlistItem.setRating(rating);
            }

            watchlistitemRepository.addItem(watchlistItem);
        }
        else{
            WatchlistItem existingWatchlistItem = watchlistitemRepository.findWatchlistItemById(watchlistItem.getId());
            watchlistitemRepository.updateItem(watchlistItem, existingWatchlistItem);
        }
    }

    public WatchlistItem findWatchlistItembyId(Integer id){
        if (id ==null){
            return new WatchlistItem();
        }
        return watchlistitemRepository.findWatchlistItemById(id);
    }

    public Boolean findIfWatchlistExists(Integer id){
        return watchlistitemRepository.findIfWatchlistExists(id);
    }


}
