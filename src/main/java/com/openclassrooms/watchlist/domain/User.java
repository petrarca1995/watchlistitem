package com.openclassrooms.watchlist.domain;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter

public class User {
    private int id;
    private String email;
    private String username;
    private String password;

    public User(){
    }
}
