DROP DATABASE IF EXISTS watchlist_app;
CREATE DATABASE watchlist_app;
USE watchlist_app;

CREATE TABLE Users (
  id int auto_increment primary key,
  username varchar(255) UNIQUE NOT NULL,
  pass varchar(255) NOT NULL
);


CREATE TABLE Movies (
  id int auto_increment primary key,
  title varchar(255) NOT NULL,
  rating varchar(255) ,
  priority varchar(3) ,
  comment varchar(300),
  user_id int NOT NULL,
  FOREIGN KEY(user_id) REFERENCES Users(id) ON DELETE CASCADE
);
